// Importations
var models = require('../models');

module.exports = {
    addClient: function(req, res) {

            var lastName=req.body.lastName;
            var firstName=req.body.firstName;
            var email=req.body.email;
            var phone=req.body.phone;
            var DateNais=req.body.DateNais;
            var Adresse=req.body.Adresse;
            
           
           models.ClientsEntity.findOne({
               where: {phone: phone}
           }) .then(function(userFound){

            if(!userFound){
              models.ClientsEntity.create({
                    lastName:lastName,
                    firstName:firstName,
                    email:email,
                    phone:phone,
                    DateNais:DateNais,
                    Adresse:Adresse,
                    isVip: false,
                    hasAccount:false
                }).then(function(newClient) {
                    models.CltRest.create({
                        isActive:1,
                        restaurantEntitiesId:1,
                        clientEntitiesId:newClient.id
                    }).then(function(newCltRest){
                        res.status(200).json({
                            'data': newCltRest,
                        });
                    })
                    /*res.status(200).json({
                        'data': newClient
                    });*/
                }).catch(function(err){
                    res.status(500).json({
                        'message': 'ERROR '+err
                    });
                })
            } else {
                res.status(400).json({
                    'message': 'client existant '
                });
            }
            

           }).catch(function(err){
            res.status(500).json({
                'message': 'ERROR '+err
            });
        })

            

    },


    getAllClients: function(req, res){
          models.ClientsEntity.findAll({
            attributes: ['lastName','firstName', 'email', 'phone','Adresse','DateNais','isVip','hasAccount','createdAt'],
            order: [ 
                ['createdAt', 'DESC'],
         
               
        ],  
           
          }).then(function(dataClient){
              res.status(200).json({
                  'message':'all client ',
                  'data': dataClient
                 });
      
          }).catch(function(err){
              res.status(500).json({
                  'message':'ERROR ' + err
      
              });
          })
      
      },


      delete: (req, res) => {
        const id = req.params.id;
        models.ClientsEntity.destroy({
                where: { id: id }
            }).then(() => {
                res.status(200).json( { msg: 'Deleted Successfully -> User Id = ' + id } );
            }).catch(err => {
                console.log(err);
                res.status(500).json({msg: "error", details: err});
            });
    }



}