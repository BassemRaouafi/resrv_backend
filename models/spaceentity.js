'use strict';
module.exports = (sequelize, DataTypes) => {
  const spaceEntity = sequelize.define('spaceEntity', {
    title: DataTypes.STRING,
    color: DataTypes.STRING,
    createDate: DataTypes.DATE,
    isDleted: DataTypes.BOOLEAN,
    deletedDate: DataTypes.DATE
  }, {});
  spaceEntity.associate = function(models) {
    // associations can be defined here
    spaceEntity.hasMany(models.ressourcesEntity);
    spaceEntity.belongsTo(models.RestaurantEntity, {
      foreignKey: 'restaurantEntitiesId'
    })
  };
  return spaceEntity;
};