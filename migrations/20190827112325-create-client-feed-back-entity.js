'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ClientFeedBackEntities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      rate: {
        type: Sequelize.INTEGER
      },
      comment: {
        type: Sequelize.STRING
      },
      restaurantEntitiesId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          
          model: 'RestaurantEntities',
          key: 'id'
        }
      },
      clientEntitiesId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'ClientsEntities',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ClientFeedBackEntities');
  }
};