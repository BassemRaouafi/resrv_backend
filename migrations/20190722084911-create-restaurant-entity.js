'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('RestaurantEntities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Nom: {
        type: Sequelize.STRING
      },
      Tel: {
        type: Sequelize.STRING
      },
      timezone: {
        type: Sequelize.STRING
      },
      URL: {
        type: Sequelize.STRING
      },
      Description: {
        type: Sequelize.STRING
      },
      Note: {
        type: Sequelize.INTEGER
      },
      Adresse: {
        type: Sequelize.STRING
      },
      position: {
        type: Sequelize.STRING
      },
      photo: {
        type: Sequelize.STRING
      },
      tol: {
        type: Sequelize.INTEGER
      },
     
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('RestaurantEntities');
  }
};