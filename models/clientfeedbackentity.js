'use strict';
module.exports = (sequelize, DataTypes) => {
  const ClientFeedBackEntity = sequelize.define('ClientFeedBackEntity', {
    rate: DataTypes.INTEGER,
    comment: DataTypes.STRING
  }, {});
  ClientFeedBackEntity.associate = function(models) {
    // associations can be defined here
    ClientFeedBackEntity.belongsTo(
      models.RestaurantEntity,
       {
      foreignKey: 'restaurantEntitiesId'
    });
    ClientFeedBackEntity.belongsTo(
      models.ClientsEntity,
       {
      foreignKey: 'clientEntitiesId'
    });
 
  };
  return ClientFeedBackEntity;
};