'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserEntity = sequelize.define('UserEntity', {
    nom: DataTypes.STRING,
    prenom: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    tel: DataTypes.STRING,
    adresse: DataTypes.STRING,
    photo: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN
  }, {});
  UserEntity.associate = function(models) {
    // associations can be defined here
    models.UserEntity.belongsTo(models.RoleEntity, {
      foreignKey: 'roleEntitiesId'
        
       
    });
    UserEntity.belongsTo(
      models.RestaurantEntity,
       {
      foreignKey: 'restaurantEntitiesId'
    }); 

  
  };
  return UserEntity;
};