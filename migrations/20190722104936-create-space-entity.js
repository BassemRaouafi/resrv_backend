'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('spaceEntities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      color: {
        type: Sequelize.STRING
      },
      createDate: {
        type: Sequelize.DATE
      },
      isDleted: {
        type: Sequelize.BOOLEAN
      },
      deletedDate: {
        type: Sequelize.DATE
      },
      restaurantEntitiesId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          
          model: 'RestaurantEntities',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('spaceEntities');
  }
};