'use strict';
module.exports = (sequelize, DataTypes) => {
  const ressourcesEntity = sequelize.define('ressourcesEntity', {
    numRessource: DataTypes.INTEGER,
    capacity: DataTypes.INTEGER,
    design: DataTypes.STRING,
    topDistance: DataTypes.INTEGER,
    lefDistance: DataTypes.INTEGER,
    isDeleted: DataTypes.BOOLEAN,
    deleteDate: DataTypes.DATE,
    ressourcesStatus: DataTypes.STRING,
    dateNonDispo: DataTypes.STRING,
    timeNonDispo: DataTypes.STRING,
  }, {});
  ressourcesEntity.associate = function(models) {
    // associations can be defined here
    ressourcesEntity.belongsTo(models.spaceEntity, {
      foreignKey: 'spaceEntitiesId'
    });
    ressourcesEntity.hasMany(models.ReservationsEntity);
  };
  return ressourcesEntity;
};