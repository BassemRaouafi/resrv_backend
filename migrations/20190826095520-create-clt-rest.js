'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CltRests', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      isActive: {
        type: Sequelize.BOOLEAN
      },
      restaurantEntitiesId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          
          model: 'RestaurantEntities',
          key: 'id'
        }
      },
      clientEntitiesId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'ClientsEntities',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CltRests');
  }
};