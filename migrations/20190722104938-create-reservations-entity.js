'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ReservationsEntities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      date: {
        type: Sequelize.STRING
      },
      timeForm: {
        type: Sequelize.STRING
      },
      timeTo: {
        type: Sequelize.STRING
      },
      qtyMen: {
        type: Sequelize.INTEGER
      },
      qtyWomen: {
        type: Sequelize.INTEGER
      },
      qtyArriveMen: {
        type: Sequelize.INTEGER
      },
      qtyArriveWomen: {
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.STRING
      },
      clientCanceled: {
        type: Sequelize.BOOLEAN
      },
      adminCanceled: {
        type: Sequelize.BOOLEAN
      },
      clientExit: {
        type: Sequelize.BOOLEAN
      },
      timeArrival: {
        type: Sequelize.STRING
      },
      comment: {
        type: Sequelize.STRING
      },
      closed: {
        type: Sequelize.BOOLEAN
      },
      restaurantEntitiesId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          
          model: 'RestaurantEntities',
          key: 'id'
        }
      },
      clientEntitiesId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'ClientsEntities',
          key: 'id'
        }
      },
      ressourcesEntitiesId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'ressourcesEntities',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ReservationsEntities');
  }
};