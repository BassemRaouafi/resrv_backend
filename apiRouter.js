// Importations
const express = require('express');
const userCtril = require('./routes/userCtrl');
const restaurantCtrl = require('./routes/restaurantCtrl');
const clientCtrl = require('./routes/clientCtrl');
const reservationCtrl = require('./routes/reservationCtrl');
//const reservationCtrl = require('./routes/reservationCtrl');
const spaceCtrl = require('./routes/spaceCtrl');
const ressourcesCtrl = require('./routes/ressourcesCtrl');
//const mailCtrl = require('./routes/mailCtrl');
const ctlrest = require('./routes/cltrest');
const avisCtrl = require('./routes/ClientFeedBackEntityCtrl')


//Routes
exports.router = (function() {
    var apiRouter = express.Router();

    //Users routes
    apiRouter.route('/users/register/').post(userCtril.register);
    apiRouter.route('/users/login/').post(userCtril.login);
    apiRouter.route('/users/all').get(userCtril.Toususer);


    //restaurant routes
    apiRouter.route('/restaurant/creation/').post(restaurantCtrl.create);
    apiRouter.route('/restaurant/touslesrestos/').get(restaurantCtrl.getRestaurant);
    apiRouter.route('/restaurant/findById/:id').get(restaurantCtrl.getRestaurantById);


   //client routes
   
   apiRouter.route('/client/add').post(clientCtrl.addClient);
  

   apiRouter.route('/client/all').get(clientCtrl.getAllClients);
   apiRouter.route('/client/delete/:id').delete(clientCtrl.delete);


   //client resto routes
   apiRouter.route('/clientRest/all').get(ctlrest.getListCltOfResto);
   apiRouter.route('/restaurant/listClients/').post(ctlrest.getClientsByResto);

   //reservation routes
   apiRouter.route('/resrv/add').post(reservationCtrl.addResrvation);
   apiRouter.route('/resrv/all').post(reservationCtrl.getAllReservation);
   apiRouter.route('/resrv/allEnt').get(reservationCtrl.getEntReservation);
  
   apiRouter.route('/resrv/delete/:id').delete(reservationCtrl.delete);
   apiRouter.route('/resrv/update/').put(reservationCtrl.updateResrv);
   apiRouter.route('/resrv/arrived/').put(reservationCtrl.confirmedArrival);

   apiRouter.route('/resrv/stat/:id').get(reservationCtrl.statRsrvByClient);
   apiRouter.route('/resrv/attr').get(reservationCtrl.getReservationByRessource);
   apiRouter.route('/resrv/attribuerTbale').put(reservationCtrl.attribuerTbale);
   apiRouter.route('/resrv/bydate').post(reservationCtrl.getResGroupeByDate);
   apiRouter.route('/resrv/byId/:id').get(reservationCtrl.getResrvById);
   
//Space routes
   apiRouter.route('/space/add').post(spaceCtrl.addSpace);
   apiRouter.route('/space/all').get(spaceCtrl.getAllspace);
   apiRouter.route('/space/delete/:id').delete(spaceCtrl.delete);

// Ressources routes
apiRouter.route('/ressources/add').post(ressourcesCtrl.addRessource);
apiRouter.route('/ressources/all').get(ressourcesCtrl.getAllressources);
apiRouter.route('/ressources/byspace/:id').get(ressourcesCtrl.getressourcesBySpace);
apiRouter.route('/ressources/byspacebydate/').post(ressourcesCtrl.getressourcesBySpaceByDate);
apiRouter.route('/ressources/update').put(ressourcesCtrl.updateById);

//avis routes
apiRouter.route('/avis/create').post(avisCtrl.addAvis);
apiRouter.route('/avis/byrate').post(avisCtrl.getAllAvisByrate);
apiRouter.route('/avis/all').get(avisCtrl.getAllAvisClient);
apiRouter.route('/avis/resto').post(avisCtrl.getAllAvisClientByResto);


//mail routes

//apiRouter.route('/mail/send').get(mailCtrl.sendmail);
    
    
    


    return apiRouter;
})();