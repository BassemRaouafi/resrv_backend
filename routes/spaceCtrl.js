var models = require('../models');


module.exports = {
    addSpace: function(req, res) {

        var title=req.body.title;
       
          models.spaceEntity.create({
                title:title,
                isDleted: false,
                restaurantEntitiesId: 1
            }).then(function(newSpace) {
                res.status(200).json({
                    'data': newSpace
                });
            }).catch(function(err){
                res.status(500).json({
                    'message': 'ERROR '+err
                });
            })     
},


getAllspace: function(req, res){
    models.spaceEntity.findAll({
        attributes: ['id','title','restaurantEntitiesId'],

        include: [
            
           
             { model:  models.RestaurantEntity ,
                 as: 'RestaurantEntity' ,
                 required: false,}
           
         ],

      }).then(function(data){
          res.status(200).json({
              'data': data
             });
  
      }).catch(function(err){
          res.status(500).json({
              'message':'ERROR ' + err
  
          });
      })
  
  },


  delete: (req, res) => {
    const id = req.params.id;
    models.spaceEntity.destroy({
            where: { id: id }
        }).then(() => {
            res.status(200).json( { msg: 'Deleted Successfully -> Space Id = ' + id } );
        }).catch(err => {
            console.log(err);
            res.status(500).json({msg: "error", details: err});
        });
},

}