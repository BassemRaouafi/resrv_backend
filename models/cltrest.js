'use strict';
module.exports = (sequelize, DataTypes) => {
  const CltRest = sequelize.define('CltRest', {
    isActive: DataTypes.BOOLEAN
  }, {});
  CltRest.associate = function(models) {
    // associations can be defined here
    CltRest.belongsTo(
      models.RestaurantEntity,
       {
      foreignKey: 'restaurantEntitiesId'
    }); 

    CltRest.belongsTo(
      models.ClientsEntity,
       {
      foreignKey: 'clientEntitiesId'
    });

  };
  return CltRest;
};