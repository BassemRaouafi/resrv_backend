var models = require('../models');
var Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = {
    addRessource: function(req, res) {

        var numRessource=req.body.numRessource;
       
          models.ressourcesEntity.create({
            numRessource:numRessource,
                isDleted: false,
                ressourcesStatus: 'disponible',
                spaceEntitiesId: 2
            }).then(function(newResource) {
                res.status(200).json({
                    'data': newResource
                });
            }).catch(function(err){
                res.status(500).json({
                    'message': 'ERROR '+err
                });
            })     
},



getAllressources: function(req, res){
    var dataRessources=  models.ressourcesEntity.findAll({
            attributes:['id','numRessource','ressourcesStatus','dateNonDispo','timeNonDispo'],
           

      }).then(function(dataRessources){
          res.status(200).json({
              'message':'all ressources ',
              'data': dataRessources
             });
  
      }).catch(function(err){
          res.status(500).json({
              'message':'ERROR ' + err
  
          });
      })
  
  },
  getressourcesBySpace: function(req, res){
          const id = req.params.id;
          models.spaceEntity.findOne({
            attributes: ['id','title','restaurantEntitiesId'],

            include: [
                
               
                 { model:  models.RestaurantEntity ,
                     as: 'RestaurantEntity' ,
                     required: false,}
               
             ],
             where: {id:id}
             

    
          }).then(function(dataSpace){
          

            var dataRessources=  models.ressourcesEntity.findAll({
                attributes:['id','numRessource','ressourcesStatus','dateNonDispo','timeNonDispo'],
                 where: [{spaceEntitiesId: dataSpace.id},{ressourcesStatus: 'disponible'}],
                 order: [ 
                    ['numRessource', 'ASC']]
    
          }).then(function(dataRessources){
           
            res.status(200).json({
                'message':'ressources ',
                'dataRssource':dataRessources,
                'dataSpace': dataSpace
    
            });
            
        
    

          }).catch(function(err){
            res.status(500).json({
                'message':'ERROR ressources ' + err
    
            });
        })
    

      }).catch(function(err){
          res.status(500).json({
              'message':'ERROR space ' + err
  
          });
      })
  
  },

  updateById: function(req,res){
    const id = req.body.id;
    const dateNonDispo=req.body.dateNonDispo;
    const timeNonDispo=req.body.timeNonDispo;
   
	models.ressourcesEntity.update(
        {dateNonDispo:dateNonDispo,
            timeNonDispo:timeNonDispo,
            updatedAt:Date}, 
            {
                 where: {id: id} 
                })
            .then(function(ressource) {
				res.status(200).json( { 
                    'ressource':ressource,
                    'message':'sayeéééé'
                   
                });
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error"+err, details: err});
			});
  },



  getressourcesBySpaceByDate: function(req, res){
    const id = req.body.id;
    const date=req.body.date
    models.spaceEntity.findOne({
      attributes: ['id','title','restaurantEntitiesId'],

      include: [
          
         
           { model:  models.RestaurantEntity ,
               as: 'RestaurantEntity' ,
               required: false,}
         
       ],
       where: [{id:id}]
       


    }).then(function(dataSpace){
    

      var dataRessources=  models.ressourcesEntity.findAll({
          attributes:['id','numRessource','ressourcesStatus','dateNonDispo','timeNonDispo'],
           where: [
               {spaceEntitiesId: dataSpace.id},{ressourcesStatus: 'disponible'},{
                [Op.or]: [
                    {dateNonDispo: { [Op.notILike]: date}},
                    {dateNonDispo: { [Op.is]: null }}
     
                 ]
               }
             
        ],
           order: [ 
              ['numRessource', 'ASC']]

    }).then(function(dataRessources){
     
      res.status(200).json({
          'message':'ressources ',
          'dataRssource':dataRessources,
          'dataSpace': dataSpace

      });
      
  


    }).catch(function(err){
      res.status(500).json({
          'message':'ERROR ressources ' + err

      });
  })


}).catch(function(err){
    res.status(500).json({
        'message':'ERROR space ' + err

    });
})

},






}