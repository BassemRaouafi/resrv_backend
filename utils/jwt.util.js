
// Importations
var jwt = require ('jsonwebtoken');
//jwt signature
const JWT_SIGN_SECRET = '0sjs6gf9mk9nwxq22pzn5hvpxmpgtty34tfx8gz17sy6djnm0xuc65bi9rcc';
//Exported functions
module.exports = {
    generateTokenForUser: function(userData) {
        return jwt.sign({
            userId: userData.id,
            //isAdmin: userData.isAdmin
        },
        JWT_SIGN_SECRET,
        {
            //date d'expiration de jwt signature (exemple aprés 1h )
            expiresIn: '1h'

        });
    },
   /* parseAuthorization: function(authorization) {

    }*/
}