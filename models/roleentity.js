'use strict';
module.exports = (sequelize, DataTypes) => {
  const RoleEntity = sequelize.define('RoleEntity', {
    description: DataTypes.STRING
  }, {});
  RoleEntity.associate = function(models) {
    // associations can be defined here
    models.RoleEntity.hasMany(models.UserEntity);
  };
  return RoleEntity;
};