'use strict';
module.exports = (sequelize, DataTypes) => {
  const ReservationsEntity = sequelize.define('ReservationsEntity', {
    //id: DataTypes.INTEGER,
    date: DataTypes.STRING,
    timeForm: DataTypes.STRING,
    timeTo: DataTypes.STRING,
    qtyMen: DataTypes.INTEGER,
    qtyWomen: DataTypes.INTEGER,
    qtyArriveMen: DataTypes.INTEGER,
    qtyArriveWomen: DataTypes.INTEGER,
    status: DataTypes.STRING,
    clientCanceled: DataTypes.BOOLEAN,
    adminCanceled: DataTypes.BOOLEAN,
    clientExit: DataTypes.BOOLEAN,
    timeArrival: DataTypes.STRING,
    comment: DataTypes.STRING,
    closed: DataTypes.BOOLEAN
    
  }, {});
  ReservationsEntity.associate = function(models) {
   
    ReservationsEntity.belongsTo(
      models.RestaurantEntity,
       {
      foreignKey: 'restaurantEntitiesId'
    }); 

    ReservationsEntity.belongsTo(
      models.ClientsEntity,
       {
      foreignKey: 'clientEntitiesId'
    });

    ReservationsEntity.belongsTo(
      models.ressourcesEntity,
       {
      foreignKey: 'ressourcesEntitiesId'
    });

  };
  return ReservationsEntity;
};