// Importations
var models = require('../models');
//var mail = require('./mailCtrl');
module.exports = {

addResrvation: function(req, res) {
    //client données
   var lastName = req.body.lastName;
   var firstName= req.body.firstName;
   var email= req.body.email;
   var phone= req.body.phone;
   
 console.log(email);
   //reservation données
   var date= req.body.date;
   var timeForm= req.body.timeForm;
   var qtyMen= req.body.qtyMen;
   var qtyWomen= req.body.qtyWomen;
   var ressourcesEntitiesId= req.body.ressourcesEntitiesId;
   var status=req.body.status;
   var closed=false;


   models.ClientsEntity.findOne({
    where: {phone: phone}
}) .then(function(userFound){

 if(!userFound){

   models.ClientsEntity.create({
         lastName:lastName,
         firstName:firstName,
         email:email,
         phone:phone,
         isVip: false,
         hasAccount:false
     }).then(function(newClient) {
        

        models.CltRest.create({
            isActive:1,
            restaurantEntitiesId:1,
            clientEntitiesId:newClient.id
        }).then(function(newCltRest){
            models.ReservationsEntity.create({
                date:date,
                timeForm:timeForm,
                qtyMen:qtyMen,
                qtyWomen:qtyWomen,
                qtyArriveMen:0,
                qtyArriveWomen:0,
                status:status,
                closed:closed,
                clientEntitiesId:newClient.id,
                restaurantEntitiesId: 1,
                ressourcesEntitiesId:ressourcesEntitiesId
            }).then(function(newReservation){
               // mail.sendmail(newClient);
                res.status(201).json({
                    'client':newClient,
                    'data': newReservation
                   });
        
            }).catch(function(err){
                res.status(500).json({
                    'message':'ERROR ' + err
        
                });
            })
        })

     /*   models.ReservationsEntity.create({
            date:date,
            timeForm:timeForm,
            qtyMen:qtyMen,
            qtyWomen:qtyWomen,
            qtyArriveMen:0,
            qtyArriveWomen:0,
            status:status,
            closed:closed,
            clientEntitiesId:newClient.id,
            restaurantEntitiesId: 1,
            ressourcesEntitiesId:ressourcesEntitiesId
        }).then(function(newReservation){
           // mail.sendmail(newClient);
            res.status(201).json({
                'client':newClient,
                'data': newReservation
               });
    
        }).catch(function(err){
            res.status(500).json({
                'message':'ERROR ' + err
    
            });
        })
*/

     }).catch(function(err){
         res.status(500).json({
             'message': 'ERROR '+err
         });
     })
 } else {

    models.CltRest.findOne({
        attributes: ['clientEntitiesId','restaurantEntitiesId', 'isActive','createdAt'],
        where: {clientEntitiesId: userFound.id}
    }).then(function(result){
        if(result){
            models.ReservationsEntity.create({
                date:date,
                timeForm:timeForm,
                qtyMen:qtyMen,
                qtyWomen:qtyWomen,
                qtyArriveMen:0,
                qtyArriveWomen:0,
                status: status,
                closed:closed,
                clientEntitiesId:userFound.id,
                restaurantEntitiesId: 1,
                ressourcesEntitiesId:ressourcesEntitiesId
            }).then(function(newReservation){
                res.status(201).json({
                    'client':userFound,
                    'data': newReservation
                   });
        
            }).catch(function(err){
                res.status(500).json({
                    'message':'ERROR ' + err
        
                });
            })

        }
        else {
            models.CltRest.create({
                isActive:1,
                restaurantEntitiesId:1,
                clientEntitiesId:userFound.id
            }).then(function(newCltRest){
                models.ReservationsEntity.create({
                    date:date,
                    timeForm:timeForm,
                    qtyMen:qtyMen,
                    qtyWomen:qtyWomen,
                    qtyArriveMen:0,
                    qtyArriveWomen:0,
                    status: status,
                    closed:closed,
                    clientEntitiesId:userFound.id,
                    restaurantEntitiesId: 1,
                    ressourcesEntitiesId:ressourcesEntitiesId
                }).then(function(newReservation){
                    res.status(201).json({
                        'client':userFound,
                        'data': newReservation
                       });
            
                }).catch(function(err){
                    res.status(500).json({
                        'message':'ERROR ' + err
            
                    });
                })
            })
        }
    })
    
          /*  models.ReservationsEntity.create({
                date:date,
                timeForm:timeForm,
                qtyMen:qtyMen,
                qtyWomen:qtyWomen,
                qtyArriveMen:0,
                qtyArriveWomen:0,
                status: status,
                closed:closed,
                clientEntitiesId:userFound.id,
                restaurantEntitiesId: 1,
                ressourcesEntitiesId:ressourcesEntitiesId
            }).then(function(newReservation){
                res.status(201).json({
                    'client':userFound,
                    'data': newReservation
                   });
        
            }).catch(function(err){
                res.status(500).json({
                    'message':'ERROR ' + err
        
                });
            })*/
    
 }
 

}).catch(function(err){
 res.status(500).json({
     'message': 'ERROR '+err
 });
})






},

getAllReservation: function(req, res){
    const restaurantEntitiesId=req.body.restaurantEntitiesId;
  var datatt=  models.ReservationsEntity.findAll({
    
    attributes: ['id','date','timeForm','qtyMen', 'qtyWomen','qtyArriveMen','qtyArriveWomen','status','closed','clientEntitiesId','restaurantEntitiesId','ressourcesEntitiesId'],
        include: [
            
           { model: models.ClientsEntity ,
            as: 'ClientsEntity' ,
            required: false,},
            { model:  models.RestaurantEntity ,
                as: 'RestaurantEntity' ,
                required: false,},
                { model:  models.ressourcesEntity ,
                    attributes: ['id','numRessource'],
                    as: 'ressourcesEntity' ,
                    required: false,},
                
                
          
        ],
        where:{restaurantEntitiesId:restaurantEntitiesId},
        order: [ 
            ['status', 'DESC'],['date', 'ASC'],
           
    ]
       
          
          
    }).then(function(data){
        
            res.status(201).json({
                'data': data,
                //'table':dataRessources
               });
        
        

    }).catch(function(err){
        res.status(500).json({
            'message':'ERROR ' + err

        });
    })

},

getEntReservation: function(req, res){
    var data=  models.ReservationsEntity.findAll({
      
      attributes: ['id','date','timeForm','qtyMen', 'qtyWomen','status','clientEntitiesId','restaurantEntitiesId'],
          include: [
              
             { model: models.ClientsEntity ,
              as: 'ClientsEntity' ,
              required: false,},
              { model:  models.RestaurantEntity ,
                  as: 'RestaurantEntity' ,
                  required: false,}
            
          ],
          
           where: {status:'en attente'},
           order: [ 
            ['updatedAt', 'ASC'],
            ['createdAt', 'ASC']
           ]
            
      }).then(function(data){
          res.status(201).json({
              'data': data
             });
  
      }).catch(function(err){
          res.status(500).json({
              'message':'ERROR ' + err
  
          });
      })
  
  },

delete: (req, res) => {
    const id = req.params.id;
    models.ReservationsEntity.destroy({
            where: { id: id }
        }).then(() => {
            res.status(200).json( { msg: 'Deleted Successfully -> Resrv Id = ' + id } );
        }).catch(err => {
            console.log(err);
            res.status(500).json({msg: "error", details: err});
        });
},

//mise à jour reservation
updateResrv : (req, res) => {
    const id = req.body.id;
    const ressourcesEntitiesId=req.body.ressourcesEntitiesId;
    //const status=req.body.status;
	models.ReservationsEntity.update(
        {status:req.body.status,
            ressourcesEntitiesId:ressourcesEntitiesId,
            updatedAt:Date}, 
            {
                 where: {id: id} 
                })
            .then(function(reservation) {
				res.status(200).json( { 
                    'reservation':reservation,
                    'message': 'réservation '+reservation.status
                });
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error", details: err});
			});
},


updateResrvTable : (req, res) => {
    const id = req.body.id;
    
    //const status=req.body.status;
	models.ReservationsEntity.update(
        {//status:req.body.status,
            ressourcesEntitiesId:req.body.ressourcesEntitiesId,
            updatedAt:Date}, 
            {
                 where: {id: id} 
                })
            .then(function(reservation) {
				res.status(200).json( { 
                    'reservation':reservation,
                    'message': 'réservation '+reservation.status
                });
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error", details: err});
			});
},



statRsrvByClient: function(req, res){
    const idcl = req.params.id;
    
    var data=  models.ReservationsEntity.findAndCountAll({
      


      attributes: ['id','date','clientEntitiesId'],
          include: [
              
             { model: models.ClientsEntity ,
              as: 'ClientsEntity' ,
              required: false,},
              { model:  models.RestaurantEntity ,
                  as: 'RestaurantEntity' ,
                  required: false,}
            
          ],
          where :{clientEntitiesId:idcl},
             
      
         
            
            
      }).then(function(data){
          res.status(201).json({
              'data': data
             });
  
      }).catch(function(err){
          res.status(500).json({
              'message':'ERROR ' + err
  
          });
      })
  
  },
  


  getReservationByRessource: function(req, res){
    var data=  models.ReservationsEntity.findAll({
      
      attributes: ['id','date','timeForm','qtyMen', 'qtyWomen','status','clientEntitiesId','restaurantEntitiesId','ressourcesEntitiesId'],
          include: [
              
             { model: models.ClientsEntity ,
              as: 'ClientsEntity' ,
              required: false,},
              { model:  models.RestaurantEntity ,
                  as: 'RestaurantEntity' ,
                  required: false,
                },
          ],
          order: [ 
              ['status', 'DESC'],
             
      ]      
      }).then(function(data){
        models.ressourcesEntity.findOne({
            attributes:['id','numRessource'],
            where:{id:1}

      }).then(function(dataRessources){
          res.status(201).json({
              'dataresrv': data,
              'dataRessources': dataRessources
             });
            })
      }).catch(function(err){
          res.status(500).json({
              'message':'ERROR ' + err
  
          });
      })
  
  },



  attribuerTbale : (req, res) => {
    const id = req.body.id;
    const ressourcesEntitiesId=req.body.ressourcesEntitiesId;
	models.ReservationsEntity.update(
        {status:'confirmée',
            ressourcesEntitiesId:ressourcesEntitiesId,
            updatedAt:Date}, 
            {
                 where: {id: id} 
                })
            .then(function(reservation) {
				res.status(200).json( { 
                    'reservation':reservation,
                    'message': 'réservation '+req.body.status
                });
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error", details: err});
			});
}, 


confirmedArrival : (req, res) => {
    const id = req.body.id;
    
	models.ReservationsEntity.update(
        {qtyArriveMen:req.body.qtyArriveMen,
            qtyArriveWomen:req.body.qtyArriveWomen,
            closed:req.body.closed,
            updatedAt:Date}, 
            {
                 where: {id: id} 
                })
            .then(function(reservation) {
				res.status(200).json( { 
                    'message':reservation,
                   
                });
			}).catch(err => {
				console.log(err);
				res.status(500).json({msg: "error", details: err});
			});
},




getResGroupeByDate: function(req, res){
      models.ReservationsEntity.findAll({
      
       
    attributes: ['id','date','timeForm','qtyMen', 'qtyWomen','qtyArriveMen','qtyArriveWomen','status','closed','clientEntitiesId','restaurantEntitiesId','ressourcesEntitiesId'],
    include: [
        
       { model: models.ClientsEntity ,
        as: 'ClientsEntity' ,
        required: false,},
        { model:  models.RestaurantEntity ,
            as: 'RestaurantEntity' ,
            required: false,},
            { model:  models.ressourcesEntity ,
                attributes: ['id','numRessource'],
                as: 'ressourcesEntity' ,
                required: false,},
            
            
      
    ],
    where:{date:req.body.date},
    order: [ 
        ['status', 'DESC'],['date', 'ASC'],
       
]
   
         
            
            
      }).then(function(data){
          res.status(201).json({
              'data': data
             });
  
      }).catch(function(err){
          res.status(500).json({
              'message':'ERROR ' + err
  
          });
      })
  
  },



  getResrvById:function(req, res){
      var id=req.params.id;
      models.ReservationsEntity.findOne({
    
        attributes: ['id','date','timeForm','qtyMen', 'qtyWomen','qtyArriveMen','qtyArriveWomen','status','closed','comment','clientEntitiesId','restaurantEntitiesId','ressourcesEntitiesId'],
            include: [
                
               { model: models.ClientsEntity ,
                as: 'ClientsEntity' ,
                required: false,},
                { model:  models.RestaurantEntity ,
                    as: 'RestaurantEntity' ,
                    required: false,},
                    { model:  models.ressourcesEntity ,
                        attributes: ['id','numRessource'],
                        as: 'ressourcesEntity' ,
                        required: false,},
                    
                    
              
            ],
            where: {id:id}
           
              
              
        }).then(function(data){
            
                res.status(201).json({
                    'data': data,
                   
                   });
            
            
    
        }).catch(function(err){
            res.status(500).json({
                'message':'ERROR ' + err
    
            });
        })
  }
}