'use strict';
module.exports = (sequelize, DataTypes) => {
  const RestaurantEntity = sequelize.define('RestaurantEntity', {
    //id: DataTypes.INTEGER,
    Nom: DataTypes.STRING,
    Tel: DataTypes.STRING,
    timezone: DataTypes.STRING,
    URL: DataTypes.STRING,
    Description: DataTypes.STRING,
    Note: DataTypes.INTEGER,
    Adresse: DataTypes.STRING,
    position: DataTypes.STRING,
    photo: DataTypes.STRING,
    tol: DataTypes.INTEGER,
    
  }, {});
  RestaurantEntity.associate = function(models) {
       RestaurantEntity.hasMany(models.ReservationsEntity);  
       RestaurantEntity.hasMany(models.spaceEntity);
       RestaurantEntity.hasMany(models.UserEntity);
      
       RestaurantEntity.hasMany(models.CltRest);
       RestaurantEntity.hasMany(models.ClientFeedBackEntity);

        
  };
  return RestaurantEntity;
};