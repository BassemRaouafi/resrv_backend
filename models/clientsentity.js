'use strict';
module.exports = (sequelize, DataTypes) => {
  const ClientsEntity = sequelize.define('ClientsEntity', {
    //id: DataTypes.INTEGER,
    lastName: DataTypes.STRING,
    firstName: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    Adresse: DataTypes.STRING,
    DateNais: DataTypes.DATE,
    isVip: DataTypes.BOOLEAN,
    hasAccount: DataTypes.BOOLEAN
  }, {});
  ClientsEntity.associate = function(models) {
    
     
         ClientsEntity.hasMany(models.ReservationsEntity);
         ClientsEntity.hasMany(models.CltRest);
         ClientsEntity.hasMany(models.ClientFeedBackEntity);
    
  };
  return ClientsEntity;
};