// Importations
var models = require('../models');

module.exports = {
    addAvis: function(req, res) {

            const rate=req.body.rate;
            const comment=req.body.comment;

            models.ClientFeedBackEntity.create({
                rate:rate,
                comment:comment,
                clientEntitiesId:18,
                restaurantEntitiesId: 1,
            }).then(function(newAvis) {
                res.status(200).json({
                    'data': newAvis,
                });
            }).catch(function(err){
                res.status(500).json({
                    'message': 'ERROR '+err
                });
            })
               
            
          
            

           
       

            

    },


    getAllAvisClient: function(req, res){
          models.ClientFeedBackEntity.findAll({
            attributes: ['rate','comment', 'restaurantEntitiesId', 'clientEntitiesId','createdAt'],
            include: [  { model: models.ClientsEntity ,
                as: 'ClientsEntity' ,
                required: false,},
                { model:  models.RestaurantEntity ,
                    as: 'RestaurantEntity' ,
                    required: false,},
                ],
            order: [ 
                ['createdAt', 'DESC'],
               
               
        ],  
           
          }).then(function(dataAvis){
              res.status(200).json({
                  'message':'all avis client ',
                  'data': dataAvis
                 });
      
          }).catch(function(err){
              res.status(500).json({
                  'message':'ERROR ' + err
      
              });
          })
      
      },

      getAllAvisClientByResto: function(req, res){
          const id=req.body.id;
        models.ClientFeedBackEntity.findAll({
          attributes: ['rate','comment', 'restaurantEntitiesId', 'clientEntitiesId','createdAt'],
          include: [  { model: models.ClientsEntity ,
            as: 'ClientsEntity' ,
            required: false,},
            { model:  models.RestaurantEntity ,
                as: 'RestaurantEntity' ,
                required: false,},
            ],
          where:{restaurantEntitiesId:id},
          order: [ 
              ['createdAt', 'DESC'],
       
             
      ],  
         
        }).then(function(dataAvis){
            res.status(200).json({
                'message':'all avis client ',
                'data': dataAvis
               });
    
        }).catch(function(err){
            res.status(500).json({
                'message':'ERROR ' + err
    
            });
        })
    
    },
    


    getAllAvisByrate: function(req, res){
       // const id=req.body.id;
        const rate=req.body.rate
      models.ClientFeedBackEntity.findAll({
        attributes: ['rate','comment', 'restaurantEntitiesId', 'clientEntitiesId','createdAt'],
        include: [  { model: models.ClientsEntity ,
          as: 'ClientsEntity' ,
          required: false,},
          { model:  models.RestaurantEntity ,
              as: 'RestaurantEntity' ,
              required: false,},
          ],
        where:[{restaurantEntitiesId:1},{rate:rate}],
        order: [ 
            ['createdAt', 'DESC'],
     
           
    ],  
       
      }).then(function(dataAvis){
          res.status(200).json({
              'message':'all avis client ',
              'data': dataAvis
             });
  
      }).catch(function(err){
          res.status(500).json({
              'message':'ERROR ' + err
  
          });
      })
  
  },
  

}