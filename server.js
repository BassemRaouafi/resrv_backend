// imports
const express = require('express');
const bodyParser = require('body-parser');
var apiRouter = require('./apiRouter').router;
const cors = require('cors');
//Instantiate serve

const app = express();

//Configuration de (Body Parser)
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//setting up the cors config
app.use(cors());

 app.use((req, res, next)=> {
  res.header('Acess-Control-Allow-origin', '*');
  res.header('Acess-Control-Allow-Headers', '*');

  if (req.method === 'OPTIONs'){
    res.header('Access-Control-Allow-Methods', 'PUT, GET, PATCH, DELETE, GET');
    return res.status(200).json({});
  }
  next();
});

//Configurer les routes
app.get('/', function(req, res) {
    res.setHeader('Content-Type', 'text/html');
   
    res.status(200).send('<h1>Bonjour sur mon serveur</h1>')
});


app.use('/api/', apiRouter);

//Lancer le serveur
app.listen(3000,function() {
    console.log('Server en écoute :)');
})