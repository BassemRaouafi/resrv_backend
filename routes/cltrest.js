// Importations
var models = require('../models');

module.exports = {
  


    getListCltOfResto: function(req, res){
          models.CltRest.findAll({
            attributes: ['clientEntitiesId','restaurantEntitiesId', 'isActive'],
            include: [
              
                { model: models.ClientsEntity ,
                 as: 'ClientsEntity' ,
                 required: false,},
                 { model:  models.RestaurantEntity ,
                     as: 'RestaurantEntity' ,
                     required: false,},
                  
               
             ],
            order: [ 
                ['createdAt', 'DESC'],
         
               
        ],  
           
          }).then(function(dataClient){
              res.status(200).json({
                  'message':'all client ',
                  'data': dataClient
                 });
      
          }).catch(function(err){
              res.status(500).json({
                  'message':'ERROR ' + err
      
              });
          })
      
      },


      delete: (req, res) => {
        const id = req.params.id;
        models.ClientsEntity.destroy({
                where: { id: id }
            }).then(() => {
                res.status(200).json( { msg: 'Deleted Successfully -> User Id = ' + id } );
            }).catch(err => {
                console.log(err);
                res.status(500).json({msg: "error", details: err});
            });
    },

    getClientsByResto: function(req, res){
        const id = req.body.id;
        //const date=req.body.date
        models.CltRest.findAll({
            attributes: ['clientEntitiesId','restaurantEntitiesId', 'isActive','createdAt'],
    
          include: [
              
             
            { model: models.ClientsEntity ,
                as: 'ClientsEntity' ,
                required: false,},
                { model:  models.RestaurantEntity ,
                    as: 'RestaurantEntity' ,
                    required: false,},
                 
             
           ],
           order: [ 
            ['createdAt', 'DESC']],
           where: [{restaurantEntitiesId:id}],
           
           
    
    
        
    
        }).then(function(data){
         
          res.status(200).json({
            
              'data':data,
             
    
          });
          
      
    
    
        }).catch(function(err){
          res.status(500).json({
              'message':'ERROR data ' + err
    
          });
      })
    
    
  
    
    },
    



}