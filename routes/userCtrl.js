// Importations
var bcrypt = require ('bcrypt');
var jwtUtils = require ('../utils/jwt.util');
var models = require('../models');
//var asyncLib = require('async');





//regex email
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
//Regex mot de passe
const PASSWORD_REGEX = /^(?=.*\d).{4,20}$/;


//Routes
module.exports = {
    register: function(req, res) {
        // récupérer les Params du requete
        var nom = req.body.nom;
        var prenom = req.body.prenom;
        var email = req.body.email;
        var password = req.body.password;
        var tel = req.body.tel;
        var adresse = req.body.adresse;
        var photo = req.body.photo;
       // var isActive = req.body.isActive;

        /* verification  parametres
        if ( nom == null || email == null || email == null || email == null || username == null || password == null) {
            return res.status(400).json({
                'message': 'paramètres manquants'
            });
        }*/
        /* verifier length username ( exp : entre 4 et 13)
        if(username.length >= 30 || username.length <=4){
            return res.status(400).json({
                'message': "mauvais nom d'utilisateur (doit être de longueur 5-12)"
            });
        }*/

        /*verfication adresse mail
        if (!EMAIL_REGEX.test(email)) {
            return res.status(400).json({
                'message': "L'email n'est pas valide"
            });
        }*/
        //verification password 
       /* if(!PASSWORD_REGEX.test(password)) {
            return res.status(400).json({
                'message': 'Le mot de passe doit comporter entre 4 et 8 chiffres et inclure au moins un chiffre. '
            });
        }*/
       
       
       /* asyncLib.waterfall([
            function(done) {
                done(null, 'variable1');
            },
            function(var1, done) {
                done(null);
            }
        ], function(err) {
            if(!err) {
            return res.status(200).json({'message': 'ok'});
        } else {
            return res.status(200).json({'message': 'error'});
        }

        });*/

        // verifier exist ou non
        models.UserEntity.findOne({
            attributes: ['id','email'],
            where: { email: email}
        })
        .then(function(userFound) {
            
            if (!userFound) {
                bcrypt.hash(password, 5, function(err, bcryptedPassword) {
                    var newUser = models.UserEntity.create({
                        nom: nom,
                        prenom: prenom,
                        email: email,
                        password: bcryptedPassword,
                        tel: tel,
                        adresse: adresse,
                        photo:photo,
                        isActive: 0,
                        roleEntitiesId: 2,
                        restaurantEntitiesId:1
                    })
                    .then(function(newUser) {
                        return res.status(201).json({
                            'user': newUser,
                        });
                    })
                    .catch(function(err) {
                        return res.status(500).json({
                            'message':'cannot add user'+err
                        });
                    })
                });
            }
            else {
                return res.status(400).json({
                    
                    'message':'user already exist'
                });
            }
        })
        .catch(function(err) {
            return res.status(500).json({
                'message':'impossible de vérifier user'
            });
        })
        

    },
    // fonction login
    login: function(req, res) {
        //params
        var email = req.body.email;
        var password = req.body.password;
        
         // verification  parametres
         /*if (email !== null || password !== null) {
            return res.status(400).json({
                'message': 'paramètres manquants'
            });
        }*/

      var userFound=  models.UserEntity.findOne({
            attributes: ['id','email','nom','roleEntitiesId','password','restaurantEntitiesId'],
            where: {email : email},
            include:[ {
                model: models.RoleEntity,
                as: 'RoleEntity',
                required: false
            },
            { model:  models.RestaurantEntity ,
                as: 'RestaurantEntity' ,
                required: false,},
        ]
        })
        .then(function(userFound){
           
            if (userFound) {
                bcrypt.compare(password,userFound.password, function(errBycrypt, resBycrypt) {
                    

                    if(resBycrypt){
                            console.log(userFound.RoleEntity.description);
                       return res.status(200).json({
                            'user': userFound,
                            'access_token': jwtUtils.generateTokenForUser(userFound),
                            'role': userFound.RoleEntity.description
                            
                           });       
                } else {
                        return res.status(500).json({
                            'message': 'invalid password'
                        });
                }
                })
            }else {
                return res.status(500).json({
                    'message': 'user not exist in DB'
                }) 
            }

        })
        .catch(function(err) {
            return res.status(500).json({
                'message': 'unable to verify user'+err
            })
        });


    },


    Toususer: function(req, res) {
        
        var dataRes=  models.UserEntity.findAll({
          attributes: ['email', 'roleEntitiesId'],
            
        include: {
          model: models.RoleEntity,
          as: 'RoleEntity',
          required: false
      }})
          .then(function(dataRes){
              res.status(201).json({
               'data': dataRes
              });
          })
          .catch(function(err) {
              res.status(400).json({
                  'message': 'ERROR '+err
                 });
  
          });
  
      },


      


}