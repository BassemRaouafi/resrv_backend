// Importations
var models = require('../models');


module.exports = {

    create: function(req, res) {
        var Nom= req.body.Nom;
        var Tel = req.body.Tel;
        var Description= req.body.Description;
        var Adresse= req.body.Adresse;
        var photo= req.body.photo;
        var tol=req.body.tol;

  
        models.RestaurantEntity.findOne(
            {attributes: ['id','Tel'],where: { Tel: Tel}
        }).then(function(restoFound){
            if(!restoFound){
                var newResto = models.RestaurantEntity.create({ 
                        Nom:Nom,
                        Tel :Tel,
                        Description:Description,
                        Adresse:Adresse,
                        photo:photo,
                        tol:tol
                }).then(function(newResto){
                    return res.status(201).json({
                        'restaurant': newResto,
                    });

                }) .catch(function(err) {
                    return res.status(500).json({
                        'message':'cannot add restaurant '+err
                    });
                })
            } else {
                return res.status(400).json({
                    
                    'message':'restaurant already exist'
                });
            }
        }).catch(function(err) {
            return res.status(500).json({
                'message':'impossible de vérifier restaurant '+err
            });
        })


    },

    getRestaurant: function(req, res) {
        var dataresto=  models.RestaurantEntity.findAll({
            attributes: ['id','Nom', 'Tel', 'Adresse','tol']
    }).then(function(dataresto){
        res.status(201).json({
            'data': dataresto
           });

    }).catch(function(err){
        res.status(500).json({
            'message':'ERROR ' + err

        });
    })
    

},

getRestaurantById: function(req, res) {
    const id=req.params.id
    var dataresto=  models.RestaurantEntity.findOne({
        attributes: ['id','Nom', 'Tel', 'Adresse','tol'],
        where:{id:id}
}).then(function(dataresto){
    res.status(201).json({
        'data': dataresto
       });

}).catch(function(err){
    res.status(500).json({
        'message':'ERROR ' + err

    });
})


}




}
