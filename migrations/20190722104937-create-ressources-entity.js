'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ressourcesEntities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      numRessource: {
        type: Sequelize.INTEGER
      },
      capacity: {
        type: Sequelize.INTEGER
      },
      design: {
        type: Sequelize.STRING
      },
      topDistance: {
        type: Sequelize.INTEGER
      },
      lefDistance: {
        type: Sequelize.INTEGER
      },
      isDeleted: {
        type: Sequelize.BOOLEAN
      },
      deleteDate: {
        type: Sequelize.DATE
      },
      ressourcesStatus: {
        type: Sequelize.STRING
      },
      dateNonDispo: {
        type: Sequelize.STRING
      },
      timeNonDispo: {
        type: Sequelize.STRING
      },
      spaceEntitiesId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'spaceEntities',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ressourcesEntities');
  }
};